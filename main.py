from fastapi import FastAPI
from app import database, models
from app.routers import users, movies, reviews, authentication

models.Base.metadata.create_all(bind=database.engine)

app = FastAPI()

app.include_router(users.router)
app.include_router(movies.router)
app.include_router(reviews.router)
app.include_router(authentication.router)
