from typing import Optional

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import jwt, JWTError
from sqlalchemy.orm import Session

from app import schemas, database, models, hashing, jwt_token

router = APIRouter(
    tags=["Authentication"],
)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")


async def get_current_user(
        token: str = Depends(oauth2_scheme),
        db: Session = Depends(database.get_db)
) -> schemas.UserOut:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, jwt_token.SECRET_KEY, algorithms=[jwt_token.ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = schemas.TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user: Optional[models.User] = db.query(models.User).filter(models.User.username == token_data.username).first()
    if user is None:
        raise credentials_exception
    return user


@router.post("/login", response_model=schemas.Token, status_code=status.HTTP_200_OK)
async def login_user(
        login: OAuth2PasswordRequestForm = Depends(),
        db: Session = Depends(database.get_db)
) -> schemas.Token:
    user: models.User = db.query(models.User).filter(models.User.username == login.username).first()
    if not (user and hashing.verify_pwd(login.password, user.password)):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Wrong username or password."
        )
    access_token = jwt_token.create_access_token(data={"sub": user.username})
    return schemas.Token(access_token=access_token, token_type="bearer")
