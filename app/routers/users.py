from typing import List, Optional

from fastapi import APIRouter, HTTPException, Depends, status
from sqlalchemy.orm import Session

from app import schemas, models, database, hashing
from .authentication import get_current_user


router = APIRouter(
    prefix="/users",
    tags=["Users"],
)


@router.get("", response_model=List[schemas.UserOut], status_code=status.HTTP_200_OK)
async def show_all_users(
        db: Session = Depends(database.get_db),
        current_user: schemas.UserOut = Depends(get_current_user),
) -> List[schemas.UserOut]:
    return db.query(models.User).all()


@router.get("/{id}", response_model=schemas.UserOut)
async def show_user(
        id: int,
        db: Session = Depends(database.get_db),
        current_user: schemas.UserOut = Depends(get_current_user),
) -> schemas.UserOut:
    user: Optional[models.User] = db.query(models.User).get(id)
    if user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"User with id = {id} does not exist."
        )
    return user


@router.post("/create", response_model=schemas.UserOut, status_code=status.HTTP_201_CREATED)
async def create_user(
        user: schemas.UserIn,
        db: Session = Depends(database.get_db),
        current_user: schemas.UserOut = Depends(get_current_user),
) -> schemas.UserOut:
    if not current_user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Lack of permissions."
        )
    user: models.User = models.User(
        username=user.username,
        email=user.email,
        is_admin=user.is_admin,
        password=hashing.pwd_context.hash(user.password),
    )
    db.add(user)
    db.commit()
    db.refresh(user)
    return user


@router.patch("/update/{id}", response_model=schemas.UserOut, status_code=status.HTTP_202_ACCEPTED)
async def update_user(
        id: int,
        username: str = None,
        email: str = None,
        db: Session = Depends(database.get_db),
        current_user: schemas.UserOut = Depends(get_current_user),
) -> schemas.UserOut:
    user: Optional[models.User] = db.query(models.User).get(id)
    if user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"User with id = {id} does not exist."
        )
    if not (current_user.is_admin or current_user.id == user.id):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Lack of permissions."
        )
    user.username = username if username else user.username
    user.email = email if email else user.email
    db.commit()
    db.refresh(user)
    return user


@router.delete("/delete/{id}", status_code=status.HTTP_200_OK)
async def delete_user(
        id: int,
        db: Session = Depends(database.get_db),
        current_user: schemas.UserOut = Depends(get_current_user),
) -> dict:
    user = db.query(models.User).get(id)
    if not current_user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Lack of permissions."
        )
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User with id = {id} does not exist."
        )
    db.delete(user)
    db.commit()
    return {
        "detail": "User successfully deleted."
    }
