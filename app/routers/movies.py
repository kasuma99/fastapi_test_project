from typing import List, Optional

from fastapi import APIRouter, HTTPException, Depends, status
from sqlalchemy.orm import Session

from app import schemas, models, database
from .authentication import get_current_user

router = APIRouter(
    prefix="/movies",
    tags=["Movies"],
)


@router.get("", response_model=List[schemas.Movie], status_code=status.HTTP_200_OK)
async def show_all_movies(
        db: Session = Depends(database.get_db),
        current_user: schemas.UserOut = Depends(get_current_user),
) -> List[schemas.Movie]:
    return db.query(models.Movie).all()


@router.get("/{id}", response_model=schemas.Movie, status_code=status.HTTP_200_OK)
async def show_movie(
        id: int,
        db: Session = Depends(database.get_db),
        current_user: schemas.UserOut = Depends(get_current_user),
) -> schemas.Movie:
    movie = db.query(models.Movie).get(id)
    if not movie:
        raise HTTPException(
            status_code=404,
            detail=f"Movie with id = {id} does not exist."
        )
    return movie


@router.post("/create", response_model=schemas.Movie, status_code=status.HTTP_201_CREATED)
async def create_movie(
        movie: schemas.Movie,
        db: Session = Depends(database.get_db),
        current_user: schemas.UserOut = Depends(get_current_user),
) -> schemas.Movie:
    if not current_user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Lack of permissions."
        )
    movie: models.Movie = models.Movie(
        title=movie.title,
        description=movie.description,
    )
    db.add(movie)
    db.commit()
    db.refresh(movie)
    return movie


@router.put("/update/{id}", response_model=schemas.Movie, status_code=status.HTTP_202_ACCEPTED)
async def update_movie(
        id: int,
        title: Optional[str] = None,
        description: Optional[str] = None,
        db: Session = Depends(database.get_db),
        current_user: schemas.UserOut = Depends(get_current_user)
) -> schemas.Movie:
    movie: models.Movie = db.query(models.Movie).get(id)
    if not current_user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Lack of permissions."
        )
    if movie is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Movie with id = {id} does not exist.",
        )
    movie.title = title if title else movie.title
    movie.description = description if description else movie.description
    db.commit()
    db.refresh(movie)
    return movie


@router.delete("/delete/{id}", status_code=status.HTTP_200_OK)
async def delete_movie(
        id: int,
        db: Session = Depends(database.get_db),
        current_user: schemas.UserOut = Depends(get_current_user)
) -> dict:
    movie: models.Movie = db.query(models.Movie).get(id)
    if not current_user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Lack of permissions."
        )
    if not movie:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Movie with id = {id} does not exist.",
        )
    db.delete(movie)
    db.commit()
    return {
        "detail": "Movie successfully deleted."
    }
