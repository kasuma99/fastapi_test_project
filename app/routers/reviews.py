from typing import List

from fastapi import APIRouter, HTTPException, Depends, status
from sqlalchemy.orm import Session

from app import schemas, database, models
from .authentication import get_current_user

router = APIRouter(
    prefix="/reviews",
    tags=["Reviews"],
)


@router.get("", response_model=List[schemas.ReviewOut], status_code=status.HTTP_200_OK)
async def show_all_reviews(
        db: Session = Depends(database.get_db),
        current_user: schemas.UserOut = Depends(get_current_user)
) -> List[schemas.ReviewOut]:
    return db.query(models.Review).all()


@router.get("/{id}", response_model=schemas.ReviewOut, status_code=status.HTTP_200_OK)
async def show_review(
        id: int,
        db: Session = Depends(database.get_db),
        current_user: schemas.UserOut = Depends(get_current_user)
) -> schemas.ReviewOut:
    review: models.Review = db.query(models.Review).get(id)
    if not review:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Review with id = {id} does not exist."
        )
    return review


@router.post("/create", response_model=schemas.ReviewOut, status_code=status.HTTP_201_CREATED)
async def create_review(
        review: schemas.ReviewIn,
        db: Session = Depends(database.get_db),
        current_user: schemas.UserOut = Depends(get_current_user)
) -> schemas.ReviewOut:
    user: models.User = db.query(models.User).get(review.user_id)
    movie: models.Movie = db.query(models.Movie).get(review.movie_id)
    if not (movie and user):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Movie with id = {review.movie_id} or User with id = {review.user_id} does not exist."
        )
    if not (current_user.is_admin or user.id == current_user.id):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Lack of permissions."
        )
    review: models.Review = models.Review(
        rating=review.rating,
        comment=review.comment,
        user=user,
        movie=movie,
    )
    db.add(review)
    db.commit()
    db.refresh(review)
    return review


@router.patch("/update/{id}", response_model=schemas.ReviewOut, status_code=status.HTTP_202_ACCEPTED)
async def update_review(
        id: int,
        rating: int,
        comment: str,
        db: Session = Depends(database.get_db),
        current_user: schemas.UserOut = Depends(get_current_user),
) -> schemas.ReviewOut:
    review: models.Review = db.query(models.Review).get(id)
    if not review:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Review with id = {id} does not exist.",
        )
    if not (current_user.is_admin or current_user.id == review.user_id):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Lack of permissions."
        )
    review.rating = rating if rating else review.rating
    review.comment = comment if comment else review.comment
    db.commit()
    db.refresh(review)
    return review


@router.delete("/delete/{id}", status_code=status.HTTP_200_OK)
async def delete_review(
        id: int,
        db: Session = Depends(database.get_db),
        current_user: schemas.UserOut = Depends(get_current_user)
) -> dict:
    review: models.Review = db.query(models.Review).get(id)
    if not review:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Review with id = {id} does not exist."
        )
    if not (current_user.is_admin or current_user.id == review.user_id):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Lack of permissions."
        )
    db.delete(review)
    db.commit()
    return {
        "detail": "Review successfully deleted.",
    }
