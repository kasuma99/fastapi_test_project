from typing import Optional

from pydantic import BaseModel


class BaseUser(BaseModel):
    username: str
    email: str
    is_admin: bool = False

    class Config:
        orm_mode = True


class UserOut(BaseUser):
    id: int


class UserIn(BaseUser):
    password: str


class Movie(BaseModel):
    id: int
    title: str
    description: str

    class Config:
        orm_mode = True


class BaseReview(BaseModel):
    rating: int
    comment: str

    class Config:
        orm_mode = True


class ReviewIn(BaseReview):
    user_id: int
    movie_id: int


class ReviewOut(BaseReview):
    user: UserOut
    movie: Movie


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None
